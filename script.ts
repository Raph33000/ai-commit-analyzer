import axios from "axios";
import dotenv from "dotenv";

dotenv.config();

interface GitLabAIAnalyzerConfig {
  gitlabToken: string;
  gitlabApiUrl: string;
  repositoryId: string;
  openaiApiKey: string;
  model: string;
}

class GitLabAIAnalyzer {
  private gitlabToken: string;
  private gitlabApiUrl: string;
  private repositoryId: string;
  private openaiApiKey: string;
  private model: string;

  constructor(config: GitLabAIAnalyzerConfig) {
    this.gitlabToken = config.gitlabToken;
    this.gitlabApiUrl = config.gitlabApiUrl;
    this.repositoryId = config.repositoryId;
    this.openaiApiKey = config.openaiApiKey;
    this.model = config.model;
  }

  private async getCommitChanges(commitSha: string): Promise<string | null> {
    try {
      console.log("Getting commit changes...");
      const uri = `${this.gitlabApiUrl}/projects/${this.repositoryId}/repository/commits/${commitSha}/diff`;
      const headers = {
        Authorization: `Bearer ${this.gitlabToken}`,
        "Content-Type": "application/json",
      };
      const response = await axios.get(uri, { headers });

      const changes = response.data;
      if (!changes || changes.length === 0) {
        console.log("No changes in the response from GitLab API.");
        return null;
      }

      const allChanges = changes.map((change: any) => change.diff).join("\n");

      console.log("Successfully retrieved commit changes.");
      return allChanges;
    } catch (error) {
      console.log(`Error in getCommitChanges: ${error}`);
      return null;
    }
  }

  private async invokeOpenAiCodeAnalysis(
    codeChanges: string
  ): Promise<string | null> {
    try {
      console.log("Invoking OpenAI code analysis...");
      const prompt = `
En tant que relecteur de code IA Français, votre rôle est d'analyser les modifications dans le cadre d'un projet de développement logiciel. 
Vous fournirez des commentaires sur les bugs potentiels et les problèmes critiques. Les modifications sont fournies dans le format standard git diff (unified diff). 
Vos responsabilités incluent :

- Analyser uniquement les lignes de code qui ont été ajoutées, modifiées ou supprimées. Par exemple, dans un git diff, il s'agit des lignes commençant par un '+' ou un '-'.
- Ignorer tout code qui n'a pas été modifié. Dans un git diff, il s'agit des lignes commençant par un ' ' (espace).
- Éviter la répétition dans vos revues si la ligne de code est correcte. Par exemple, si la même ligne de code apparaît plusieurs fois dans le diff, vous ne devez la commenter qu'une seule fois.
- Ne pas tenir compte de l'absence de nouvelle ligne à la fin de tous les fichiers. Cela est généralement représenté dans un git diff par '\\ No newline at end of file'.
- Utiliser des bullet points pour plus de clarté si vous avez plusieurs commentaires.
- Utiliser Markdown pour formater efficacement vos commentaires. Par exemple, vous pouvez utiliser des backticks pour formater les extraits de code.
- Écrire 'EMPTY_CODE_REVIEW' s'il n'y a pas de bugs ou de problèmes critiques identifiés.
- S'abstenir d'écrire 'EMPTY_CODE_REVIEW' s'il y a des bugs ou des problèmes critiques.

Voici les modifications de code :
${codeChanges}
      `;

      const body = {
        model: this.model,
        messages: [
          {
            role: "system",
            content: `
En tant qu'assistant IA, votre rôle est de fournir une revue de code détaillée. 
Analysez les modifications de code fournies, identifiez les problèmes potentiels, proposez des améliorations et respectez les meilleures pratiques de codage. 
Votre analyse doit être approfondie et prendre en compte tous les aspects du code, y compris la syntaxe, la logique, l'efficacité et le style.
            `,
          },
          {
            role: "user",
            content: prompt,
          },
        ],
      };

      const headers = {
        Authorization: `Bearer ${this.openaiApiKey}`,
        "Content-Type": "application/json",
      };

      const response = await axios.post(
        "https://api.openai.com/v1/chat/completions",
        body,
        { headers }
      );

      if (!response.data.choices || !response.data.choices.length) {
        console.log("No choices in the response from OpenAI API.");
        return null;
      }

      const analysis = response.data.choices[0].message.content;
      console.log("Successfully invoked OpenAI code analysis.");
      return analysis;
    } catch (error) {
      console.log(`Error in invokeOpenAiCodeAnalysis: ${error}`);
      return null;
    }
  }
  private async postCommentToMergeRequest(
    mergeRequestId: string,
    comment: string
  ): Promise<boolean> {
    try {
      console.log("Posting comment to merge request...");
      const uri = `${this.gitlabApiUrl}/projects/${this.repositoryId}/merge_requests/${mergeRequestId}/notes`;
      const headers = {
        Authorization: `Bearer ${this.gitlabToken}`,
        "Content-Type": "application/json",
      };
      const body = { body: comment };

      const response = await axios.post(uri, body, { headers });

      if (response.status !== 201) {
        console.log(`Error posting comment: ${response.status}`);
        return false;
      }

      console.log("Successfully posted comment to merge request.");
      return true;
    } catch (error) {
      console.log(`Error in postCommentToMergeRequest: ${error}`);
      return false;
    }
  }

  public async run(): Promise<void> {
    try {
      console.log("Starting main script...");
      const commitSha = process.env.CI_COMMIT_SHA;
      if (!commitSha) {
        console.log("Commit SHA not found.");
        return;
      }
      console.log(`Commit SHA is: ${commitSha}`);

      console.log("Starting main script...");

      const mergeRequestId = process.env.CI_MERGE_REQUEST_IID;

      if (!mergeRequestId) {
        console.log("Merge request ID not found.");
        return;
      }

      console.log(`MergeRequest is: ${mergeRequestId}`);

      const changes = await this.getCommitChanges(commitSha);
      if (!changes) return;

      const analysis = await this.invokeOpenAiCodeAnalysis(changes);
      if (!analysis) return;

      if (analysis.trim()) {
        await this.postCommentToMergeRequest(
          mergeRequestId,
          `Analyse de code par IA:\n${analysis}`
        );
      }

      console.log("Finished main script.");
    } catch (error) {
      console.log(`Error in main script: ${error}`);
    }
  }
}

const config: GitLabAIAnalyzerConfig = {
  gitlabToken: process.env.GITLAB_TOKEN || "",
  gitlabApiUrl: process.env.CI_API_V4_URL || "",
  repositoryId: process.env.CI_PROJECT_ID || "",
  openaiApiKey: process.env.OPENAI_API_KEY || "",
  model: process.env.GPT_MODEL || "gpt-4-turbo", // Or any other model you want to use
};

const analyzer = new GitLabAIAnalyzer(config);
analyzer.run();
