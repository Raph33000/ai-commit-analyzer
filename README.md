# AI Commit Analyzer

## Introduction

AI Commit Analyzer est un projet permettant de faire une code review via IA

Cela prend la forme d'une image docker qui contient un petit programme qui va recuperer les diff du commit actuel, et l'envoyer à chat GPT en lui demandant d'analyser le commit, puis cela créera un commentaire dans la merge request reliée avec les details retournés par chatGPT

## Comment utiliser la review automatique

Il suffit d'ajouter un nouveau stage dans son fichier .gitlab-ci.yml :

```
stages:
  - code_analysis

code_analysis:
  image: registry.gitlab.com/raph33000/ai-commit-analyzer:latest
  stage: code_analysis
  script:
     - npx ts-node /app/script.ts
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "qa"'
```

Changer la rule si nécessaire, si vous souhaitez exécuter la review lorsque une MR est effectuée sur une autre branche.

Ensuite, ajoutez dans vos variables de gitlab CI/CD, la variable suivante :

```
OPENAI_API_KEY  (et mettre en valeur une clé valide de l'api OpenAI)
GITLAB_TOKEN  (un access token qui a le full accès sur le projet dans lequel vous voulez lancer l'analyzer)
```

[Voir ici](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) pour générer un access token

Vous pouvez aussi choisir votre model de GPT en ajoutant cette variable :

```
GPT_MODEL  (si vide, par défaut on utilise gpt-4-turbo)
```

Et c'est tout !

Maintenant, à chaque fois qu'une MR sera créée vers QA, ou qu'un commit sera fait dans la MR existante, un commentaire de revue de code sera ajouté dans la MR avec différentes informations pertinentes.

## Maintenir le projet

Le projet est réalisé en typescript avec node dans un seul fichier (un peu degeu pour l'instant).

A chaque fois qu'un push sur main est effectué, cela va créer une nouvelle image qui sera push sur le registry du projet gitlab (c'est d'ailleurs cette image qu'on utilise dans le stage qu'on rajoute à son projet).

L'image comporte pour le moment seulement :

- Un version de node 20
- Le fichier script et les nodes modules installés dans le dossier /app

Il sera donc possible de faire évoluer ce projet pour lui rajouter des fonctionnalités et le rendre un peu plus stable (génération de release notes par exemple)

Pour l'installer en local :

```
git clone git@gitlab.com:Raph33000/ai-commit-analyzer.git
```

Créer un fichier .env qui comportera les variables suivantes :

```
GITLAB_TOKEN=UN GITLAB TOKEN QUI VOUS PERMET D'ACCEDER A CE PROJET
CI_API_V4_URL=https://gitlab.com/api/v4/
CI_PROJECT_ID=58159522
OPENAI_API_KEY=UNE CLE OPEN AI VALIDE
CI_MERGE_REQUEST_IID=L'ID D'UNE MR DE TEST
CI_COMMIT_SHA=LE SHA D'UN COMMIT DE TEST
GPT_MODEL=gpt-4-turbo (optionnel)
```

Ensuite lancez

```
pnpm i
```

Puis vous pouvez executer le script tout simplement avec :

```
npx ts-node /app/script.ts
```

Tadaaa

## Authors

- [@raph_pe](https://www.linkedin.com/in/rapha%C3%ABl-perchec-1726b682/)
